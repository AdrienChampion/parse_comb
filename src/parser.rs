#![doc="
Contains all the parsing functions and macros used by the library.
"]

use std::fmt ;
use std::result ;
use std::rc::Rc ;

use regex ;

use ::lexer::Lexer       ;
use ::lexer::LexerCursor ;
use ::lexer::Token       ;
use ::lexer::Pos         ;

use self::ParseFailure::{ Msg, EoF } ;
use self::Result::{ Ok, Err } ;




// |===| Static strings and error messages.



/// Message returned by the failure parser.
#[allow(non_upper_case_globals)]
static failure_parser_msg: & 'static str =
    "failure parser always fails" ;

/// Token mismatch tag.
#[allow(non_upper_case_globals)]
static token_mismatch_tag: & 'static str =
    "token mismatch" ;

/// Returns the error message for a token mismatch.
fn token_mismatch<In1, In2>(
    expected: In1, found: In2
) -> String
where In1: fmt::Display, In2: fmt::Display {
    format!(
        "[{}] expected \"{}\", found \"{}\".",
        token_mismatch_tag, expected, found
    )
}




// |===| Parser types and functions.




/// Result of a parse attempt.
pub enum Result<T> {
    /// Positive result, contains the result and the new state of the
    /// lexer.
    Ok( T, LexerCursor ),
    /// Error containing a parse failure.
    Err( ParseFailure ),
}

impl<T> fmt::Display for Result<T> {
    fn fmt(
        & self, fmt: & mut fmt::Formatter
    ) -> fmt::Result {
        match * self {
            Ok( _, _ ) => write!(
                fmt, "success"
            ),
            Err( ref failure ) => write!(
                fmt, "failure: {}", failure
            ),
        }
    }
}

/// Failed parse attempt.
pub enum ParseFailure {
    /// A wrapper around a position and a string.
    Msg( Pos, String ),
    /// End of file.
    EoF,
}

impl fmt::Display for ParseFailure {
    fn fmt(
        & self, fmt: & mut fmt::Formatter
    ) -> fmt::Result {
        match * self {
            Msg( ref pos, ref msg ) => write!(
                fmt, "{} {}", pos, msg
            ),
            EoF => write!(
                fmt, "end of file"
            ),
        }
    }
}

/// Parses a lexer using `parser`.
pub fn parse<Out, F>(
    lexer: Lexer,
    parser: F,
) -> result::Result<Out, ParseFailure>
where F: Fn(& LexerCursor) -> Result<Out> {
    let cursor = LexerCursor::mk( Rc::new(lexer) ) ;
    match parser(& cursor) {
        Ok( out, _ ) => result::Result::Ok( out ),
        Err( failure ) => result::Result::Err( failure ),
    }
}

/// Parses a lexer using `parser`. Fails if the lexer is not
/// completely consumed by `parser`.
pub fn parse_all<Out, F>(
    lexer: Lexer,
    parser: F,
) -> result::Result<Out, ParseFailure>
where F: Fn(& LexerCursor) -> Result<Out> {
    let cursor = LexerCursor::mk( Rc::new(lexer) ) ;
    match parser(& cursor) {
        Ok( out, cursor ) => {
            match cursor.read() {
                None => result::Result::Ok( out ),
                Some(token) => result::Result::Err( Msg(
                    token.pos,
                    format!(
                        "expected EoF but found {}",
                        token.tkn
                    )
                ) ),
            }
        },
        Err( failure ) =>
            result::Result::Err( failure ),
    }
}




/// A parser that always succeeds.
pub fn success(
    lexer: & LexerCursor
) -> Result<()> {
    Ok( (), lexer.cp() )
}

/// A parser that always fails.
pub fn failure(
    lexer: & LexerCursor
) -> Result<()> {
    match lexer.pos() {
        None => Err(EoF),
        Some(pos) => Err( Msg(
            pos, failure_parser_msg.to_string()
        ) ),
    }
}

/// Token parser.
pub fn token(
    lexer: & LexerCursor,
    string: & 'static str,
) -> Result<Token> {

    match lexer.read() {

        None => Err(EoF),

        Some( token ) => {
            if token.tkn == string {
                Ok( token.clone(), lexer.eat() )
            } else {
                Err( Msg(
                    token.pos,
                    token_mismatch(string, token.tkn)
                ) )
            }
        }
    }
}

/// Builds a regex.
///
/// **TODO**: replace with compiler plugin invocation asap.
pub fn build_regex( string: & str ) -> regex::Regex {
    regex::Regex::new( string ).unwrap()
}

/// Regex parser.
pub fn regex(
    lexer: & LexerCursor,
    re: regex::Regex
) -> Result<Token> {

    match lexer.read() {

        None => Err(EoF),

        Some( token ) => {
            if re.is_match(& token.tkn) {
                Ok( token.clone(), lexer.eat() )
            } else {
                Err( Msg(
                    token.pos,
                    token_mismatch(re, token.tkn)
                ) )
            }
        }
    }
}

/// Parses `left`, `mid` and `right`. Returns only `mid`.
pub fn ignr_outter<OutL, FL, Out, F, OutR, FR>(
    lexer: & LexerCursor,
    left: FL,
    mid: F,
    right: FR,
) -> Result<Out>
where
FL: Fn(& LexerCursor) -> Result<OutL>,
F:  Fn(& LexerCursor) -> Result<Out >,
FR: Fn(& LexerCursor) -> Result<OutR> {
    match seq::seq3(& lexer, left, mid, right) {
        Err( failure ) => Err( failure ),
        Ok( (_, mid, _), lexer ) => Ok( mid, lexer ),
    }
}

/// Parses an option of a parser.
/// Always succeeds.
pub fn opt<Out, F>(
    lexer: & LexerCursor,
    parser: F,
) -> Result<Option<Out>>
where F: Fn(& LexerCursor) -> Result<Out> {

    match parser(lexer) {
        Err( _ ) => Ok( None, lexer.cp() ),
        Ok( out, nu_lexer ) => Ok( Some(out), nu_lexer ),
    }
}

/// Parses a potentially empty repetition of a parser.
/// Always succeeds.
pub fn rep<Out, F>(
    lexer: & LexerCursor,
    parser: F,
) -> Result<Vec<Out>>
where F: Fn(& LexerCursor) -> Result<Out> {

    // Vector storing the result.
    let mut vec = Vec::new() ;

    // The current lexer.
    let mut lexer = lexer.cp() ;

    loop {
        match parser(& lexer) {
            Err( _ ) =>
                return Ok( vec, lexer.cp() ),
            Ok( out, nu_lexer ) => {
                vec.push(out) ;
                lexer = nu_lexer.cp()
            },
        }
    }
}

/// Parses a non-empty repetition of a parser.
pub fn rep1<Out, F>(
    lexer: & LexerCursor,
    parser: F,
) -> Result<Vec<Out>>
where F: Fn(& LexerCursor) -> Result<Out> {

    // Vector storing the result.
    let mut vec = Vec::new() ;

    // The current lexer.
    let mut lexer = lexer.cp() ;

    loop {
        match parser(& lexer) {
            Err( failure ) => {
                // Fail if vec is empty.
                if vec.len() > 0 {
                    return Ok( vec, lexer.cp() )
                } else {
                    return Err( failure )
                }
            }
            Ok( out, nu_lexer ) => {
                vec.push(out) ;
                lexer = nu_lexer.cp()
            },
        }
    }
}

/// Parses `parser` and applies `f` to the result.
pub fn fun<Out1, F1, Out2, F2>(
    lexer: & LexerCursor,
    parser: F1,
    f: F2
) -> Result<Out2>
where F1: Fn(& LexerCursor) -> Result<Out1>,
F2: Fn(Out1) -> Out2 {

    match parser(lexer) {
        Err( failure ) => Err( failure ),
        Ok( out1, lexer ) => Ok( f(out1), lexer.cp() ),
    }
}




// |===| Combinator parsers macros.

// You might want to skip over the `parser_seq` and `parser_branch`
// macro on your first read...




/// Expands the body of a `parser_seq` parser.
///
/// The `parser_seq` macro calls this macro with a list of parsers
/// to try in a nested fashion. As soon as an error occurs, the
/// parser fails.
///
/// The difficulty is to be able to refer to previous results when
/// returning the final result. The trick is that the result of
/// `$parser` is also called `$parser` and is passed down in the
/// `$( $other ),+` enumeration.
///
/// # Parameters
///
/// * `$lexer` - is the lexer passed down,
/// * `$parser` - is the current parser the macro is handling,
/// * `$parsers` - (if any) is the tail of the parsers to handle,
/// * `$other` - (if any) is the prefix of the result tuple.
///
macro_rules! parser_seq_body {
    (
        $lexer: ident |
        $parser:ident |
        $( $other:ident ),+
    ) => (
        match $parser($lexer) {
            Ok( last, lexer ) => Ok(
                ($($other),+ , last),
                lexer
            ),
            Err( failure ) =>
                Err( failure ),
        }
    ) ;
    (
        $lexer:ident |
        $parser:ident, $( $parsers:ident ),+ |
        $( $other:ident ),+
    ) => (
        match $parser($lexer) {
            Ok( $parser, lexer ) => {
                let lexer = & lexer ;
                parser_seq_body!(
                    lexer |
                    $( $parsers ),+ |
                    $( $other ),+ , $parser
                )
            },
            Err( failure ) =>
                Err( failure ),
        }
    ) ;
    (
        $lexer:ident |
        $parser:ident, $( $parsers:ident ),+
    ) => (
        match $parser($lexer) {
            Ok( $parser, lexer ) => {
                let lexer = & lexer ;
                parser_seq_body!(
                    lexer |
                    $( $parsers ),+ |
                    $parser
                )
            },
            Err( failure ) =>
                Err( failure ),
        }
    ) ;
}

/// Creates a parser_seq function combining `n` parsers.
/// `$cardinal` is only there for documentation purposes.
macro_rules! parser_seq {
    (
        $name:ident $cardinal:ident
        ( $( $parser:ident, $ty:ident, $f:ident ),+ )
    ) => (
        /// Combines $cardinal parsers in sequence.
        pub fn $name<$( $ty, $f ),+>(
            lexer: & LexerCursor,
            $( $parser : $f ),+
        ) -> Result<( $( $ty ),+ )>
        where $( $f : Fn(& LexerCursor) -> Result<$ty> ),+ {
            parser_seq_body!( lexer | $( $parser ),+ )
        }
    ) ;
}

/// Aggregates the parsers obtained by combining several parsers in
/// sequence.
pub mod seq {

    use ::lexer::LexerCursor ;
    use super::Result ;
    use super::Result::* ;

    parser_seq! {
        seq2 two (
            p1, Out1, F1,
            p2, Out2, F2
        )
    }

    parser_seq! {
        seq3 three (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3
        )
    }

    parser_seq! {
        seq4 four (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4
        )
    }

    parser_seq! {
        seq5 five (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4,
            p5, Out5, F5
        )
    }

    parser_seq! {
        seq6 six (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4,
            p5, Out5, F5,
            p6, Out6, F6
        )
    }

    parser_seq! {
        seq7 seven (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4,
            p5, Out5, F5,
            p6, Out6, F6,
            p7, Out7, F7
        )
    }

    parser_seq! {
        seq8 eight (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4,
            p5, Out5, F5,
            p6, Out6, F6,
            p7, Out7, F7,
            p8, Out8, F8
        )
    }

    parser_seq! {
        seq9 nine (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4,
            p5, Out5, F5,
            p6, Out6, F6,
            p7, Out7, F7,
            p8, Out8, F8,
            p9, Out9, F9
        )
    }

    parser_seq! {
        seq10 ten (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4,
            p5, Out5, F5,
            p6, Out6, F6,
            p7, Out7, F7,
            p8, Out8, F8,
            p9, Out9, F9,
            p10, Out10, F10
        )
    }

    parser_seq! {
        seq11 ten (
            p1, Out1, F1,
            p2, Out2, F2,
            p3, Out3, F3,
            p4, Out4, F4,
            p5, Out5, F5,
            p6, Out6, F6,
            p7, Out7, F7,
            p8, Out8, F8,
            p9, Out9, F9,
            p10, Out10, F10,
            p11, Out11, F11
        )
    }

}




// |===| Branching parsers macros.



/// Expands the body of a `parser_branch`.
macro_rules! parser_branch_body {
    ( $lexer:ident | $parser:ident ) => (
        $parser($lexer)
    ) ;
    (
        $lexer:ident | $parser:ident ,
        $( $parsers:ident ),+
    ) => (
        match $parser($lexer) {
            Err( _ ) => {
                parser_branch_body!(
                    $lexer | $( $parsers ),+
                )
            },
            ok => ok,
        }
    ) ;
}


/// Creates a `parser_branch` function combining n parsers. $cardinal is only
/// there for documentation purposes.
macro_rules! parser_branch {
    (
        $name:ident $cardinal:ident : $ty:ident
        ( $( $parser:ident : $f:ident ),+ )
    ) => (
        /// Combines $cardinal parsers in a branching parser.
        pub fn $name<$ty, $( $f ),+>(
            lexer: & LexerCursor,
            $( $parser : $f ),+
        ) -> Result<$ty>
        where $( $f: Fn(& LexerCursor) -> Result<Out> ),+ {
            parser_branch_body!(lexer | $( $parser ),+)
        }
    ) ;
}


/// Aggregates the parsers obtained by combining several parsers
/// in branches.
pub mod branch {

    use ::lexer::LexerCursor ;
    use super::Result ;
    use super::Result::Err ;

    parser_branch! {
        branch2 two: Out (
            p1: F1, p2: F2
        )
    }

    parser_branch! {
        branch3 three: Out (
            p1: F1, p2: F2, p3: F3
        )
    }

    parser_branch! {
        branch4 four: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4
        )
    }

    parser_branch! {
        branch5 five: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4, p5: F5
        )
    }

    parser_branch! {
        branch6 six: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4, p5: F5, p6: F6
        )
    }

    parser_branch! {
        branch7 seven: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4, p5: F5, p6: F6,
            p7: F7
        )
    }

    parser_branch! {
        branch8 eight: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4, p5: F5, p6: F6,
            p7: F7, p8: F8
        )
    }

    parser_branch! {
        branch9 nine: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4, p5: F5, p6: F6,
            p7: F7, p8: F8, p9: F9
        )
    }

    parser_branch! {
        branch10 ten: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4, p5: F5, p6: F6,
            p7: F7, p8: F8, p9: F9,
            p10: F10
        )
    }

    parser_branch! {
        branch11 ten: Out (
            p1: F1, p2: F2, p3: F3,
            p4: F4, p5: F5, p6: F6,
            p7: F7, p8: F8, p9: F9,
            p10: F10, p11: F11
        )
    }

}



// |===| Helper macros.


/// Parser `try` convenience macro.
#[macro_export]
macro_rules! parse_try {
  ($e:expr) => (
    match $e {
      $crate::parser::Result::Ok(res, lexer) => (res, lexer),
      $crate::parser::Result::Err(e) =>
        return $crate::parser::Result::Err(e),
    }
  )
}

/// Builds a `parser::Result::Ok` result from the value created and the lexer.
#[macro_export]
macro_rules! ok_parse {
  ($val:expr, $lex:expr) => ($crate::parser::Result::Ok(val, lexer))
}


// |===| Parser creation macros.




/// Parser combinator macro, creates a new parser.
#[macro_export]
macro_rules! parser {

    (
        fn $name:ident : $out:ty {
            $( $tt:tt )+
        }
    ) => (
        fn $name(
            lexer: & $crate::lexer::LexerCursor
        ) -> $crate::parser::Result<$out> {
            (parser!( $( $tt )+ ))(lexer)
        }
    ) ;

    // Token.
    ( [$tkn:expr] ) => (
        |lex| $crate::parser::token( lex, $tkn )
    ) ;

    // Regex.
    ( {$tkn:expr} ) => (
        |lex| $crate::parser::regex(
            lex,
            $crate::parser::build_regex($tkn)
        )
    ) ;

    // Fun.
    (
        $tt:tt => $param:pat => $block:block
    ) => (
        |lex| $crate::parser::fun(
            lex,
            parser!( $tt ),
            | $param | $block
        )
    ) ;

    // Opt.
    ( $tt:tt ? ) => (
        |lex| $crate::parser::opt( lex, parser!( $tt ) )
    ) ;

    // Rep.
    ( $tt:tt * ) => (
        |lex| $crate::parser::rep( lex, parser!( $tt ) )
    ) ;


    // Rep1.
    ( $tt:tt + ) => (
        |lex| $crate::parser::rep1( lex, parser!( $tt ) )
    ) ;

    // Ignore outter.
    (
        $tt_ignrl:tt
        > $tt_keep:tt <
        $tt_ignrr:tt
    ) => (
        |lex| $crate::parser::ignr_outter(
            lex,
            parser!( $tt_ignrl ),
            parser!( $tt_keep  ),
            parser!( $tt_ignrr )
        )
    ) ;

    // Ignore right.
    (
        $tt_keep:tt <
        $tt_ignrr:tt
    ) => (
        |lex| $crate::parser::ignr_outter(
            lex,
            $crate::parser::success,
            parser!( $tt_keep  ),
            parser!( $tt_ignrr )
        )
    ) ;

    // Ignore left.
    (
        $tt_ignrl:tt >
        $tt_keep:tt
    ) => (
        |lex| $crate::parser::ignr_outter(
            lex,
            parser!( $tt_ignrl ),
            parser!( $tt_keep  ),
            $crate::parser::success
        )
    ) ;



    // Comb2.
    (
        $tt1:tt,
        $tt2:tt
    ) => (
        |lex| $crate::parser::seq::seq2(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 )
        )
    ) ;
    // Comb3.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt
    ) => (
        |lex| $crate::parser::seq::seq3(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 )
        )
    ) ;
    // Comb4.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt
    ) => (
        |lex| $crate::parser::seq::seq4(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 )
        )
    ) ;
    // Comb5.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt,
        $tt5:tt
    ) => (
        |lex| $crate::parser::seq::seq5(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 )
        )
    ) ;
    // Comb6.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt,
        $tt5:tt,
        $tt6:tt
    ) => (
        |lex| $crate::parser::seq::seq6(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 )
        )
    ) ;
    // Comb7.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt,
        $tt5:tt,
        $tt6:tt,
        $tt7:tt
    ) => (
        |lex| $crate::parser::seq::seq7(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 )
        )
    ) ;
    // Comb8.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt,
        $tt5:tt,
        $tt6:tt,
        $tt7:tt,
        $tt8:tt
    ) => (
        |lex| $crate::parser::seq::seq8(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 )
        )
    ) ;
    // Comb9.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt,
        $tt5:tt,
        $tt6:tt,
        $tt7:tt,
        $tt8:tt,
        $tt9:tt
    ) => (
        |lex| $crate::parser::seq::seq9(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 ),
            parser!( $tt9 )
        )
    ) ;
    // Comb10.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt,
        $tt5:tt,
        $tt6:tt,
        $tt7:tt,
        $tt8:tt,
        $tt9:tt,
        $tt10:tt
    ) => (
        |lex| $crate::parser::seq::seq10(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 ),
            parser!( $tt9 ),
            parser!( $tt10 )
        )
    ) ;

    // Comb11.
    (
        $tt1:tt,
        $tt2:tt,
        $tt3:tt,
        $tt4:tt,
        $tt5:tt,
        $tt6:tt,
        $tt7:tt,
        $tt8:tt,
        $tt9:tt,
        $tt10:tt,
        $tt11:tt
    ) => (
        |lex| $crate::parser::seq::seq10(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 ),
            parser!( $tt9 ),
            parser!( $tt10 ),
            parser!( $tt11 )
        )
    ) ;

    // Branch2.
    (
        | $tt1:tt
        | $tt2:tt
    ) => (
        |lex| $crate::parser::branch::branch2(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 )
        )
    ) ;
    // Branch3.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
    ) => (
        |lex| $crate::parser::branch::branch3(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 )
        )
    ) ;
    // Branch4.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
    ) => (
        |lex| $crate::parser::branch::branch4(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 )
        )
    ) ;
    // Branch5.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
        | $tt5:tt
    ) => (
        |lex| $crate::parser::branch::branch5(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 )
        )
    ) ;
    // Branch6.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
        | $tt5:tt
        | $tt6:tt
    ) => (
        |lex| $crate::parser::branch::branch6(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 )
        )
    ) ;
    // Branch7.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
        | $tt5:tt
        | $tt6:tt
        | $tt7:tt
    ) => (
        |lex| $crate::parser::branch::branch7(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 )
        )
    ) ;
    // Branch8.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
        | $tt5:tt
        | $tt6:tt
        | $tt7:tt
        | $tt8:tt
    ) => (
        |lex| $crate::parser::branch::branch8(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 )
        )
    ) ;
    // Branch9.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
        | $tt5:tt
        | $tt6:tt
        | $tt7:tt
        | $tt8:tt
        | $tt9:tt
    ) => (
        |lex| $crate::parser::branch::branch9(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 ),
            parser!( $tt9 )
        )
    ) ;
    // Branch10.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
        | $tt5:tt
        | $tt6:tt
        | $tt7:tt
        | $tt8:tt
        | $tt9:tt
        | $tt10:tt
    ) => (
        |lex| $crate::parser::branch::branch10(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 ),
            parser!( $tt9 ),
            parser!( $tt10 )
        )
    ) ;

    // Branch11.
    (
        | $tt1:tt
        | $tt2:tt
        | $tt3:tt
        | $tt4:tt
        | $tt5:tt
        | $tt6:tt
        | $tt7:tt
        | $tt8:tt
        | $tt9:tt
        | $tt10:tt
        | $tt11:tt
    ) => (
        |lex| $crate::parser::branch::branch11(
            lex,
            parser!( $tt1 ),
            parser!( $tt2 ),
            parser!( $tt3 ),
            parser!( $tt4 ),
            parser!( $tt5 ),
            parser!( $tt6 ),
            parser!( $tt7 ),
            parser!( $tt8 ),
            parser!( $tt9 ),
            parser!( $tt10 ),
            parser!( $tt11 )
        )
    ) ;

    // Un-paren.
    ( ( $( $tt:tt )+ ) ) => ( parser!( $( $tt )+ ) ) ;

    // Ident.
    ( $id:ident ) => (|lex| $id(lex)) ;
}


