#![doc="
A lexer is a vector of token, which is a position info and a string.
A cursor on a lexer is an index and a `Rc` to a lexer.

# Examples

A lexer can be created from a file, a string, or a vector of lines.

```rust
# use parse_comb::lexer::* ;

let string = \"bla {\n  bli\n}\".to_string() ;
let lexer = Lexer::of_string(
  & string, & simple_is_whitespace
) ;
assert!( lexer.len() == 4 ) ;
```
"]


use std::fs::File ;
use std::path::Path ;
use std::io::prelude::Read ;
use std::fmt ;
use std::ops ;
use std::rc::Rc ;


/// A helper function for whitespaces. Returns true if `ch4r` is
/// ' ', '\n' or '\t'.
pub fn simple_is_whitespace(ch4r: char) -> bool {
    match ch4r {
        ' ' | '\n' | '\t' => true,
        _ => false,
    }
}

/// A position is a row and a column.
#[derive(Clone,Debug)]
pub struct Pos {
    /// Row index. Starts at one.
    pub row: usize,
    /// Column index. Start at one.
    pub col: usize
}
impl Pos {
    /// Creates a new position.
    pub fn of_ints( row: usize, col: usize ) -> Self {
        Pos { row: row, col: col }
    }
}

impl PartialEq<Pos> for Pos {
    fn eq( & self, rhs: & Pos ) -> bool {
        ( self.row == rhs.row ) && ( self.col == rhs.col )
    }
    fn ne( & self, rhs: & Pos ) -> bool {
        ( self.row != rhs.row ) || ( self.col != rhs.col )
    }
}

impl fmt::Display for Pos {
    fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
        write!(fmt, "[l{:_<3}c{:_<3}]", self.row, self.col)
    }
}

/// A token is a position `Pos` and a `String`.
#[derive(Clone,Debug)]
pub struct Token {
    /// Position of the token.
    pub pos: Pos,
    /// String representation of the token.
    pub tkn: String
}
impl Token {
    /// Creates a new token from a position and a string representation.
    pub fn of_pos( pos: Pos, tkn: String ) -> Self {
        Token { pos: pos, tkn: tkn }
    }
    /// Creates a new token from a row, a column and a string representation.
    pub fn of_ints( row: usize, col: usize, tkn: String ) -> Self {
        Token { pos: Pos::of_ints(row, col), tkn: tkn }
    }
}

impl PartialEq<Token> for Token {
    fn eq( & self, rhs: & Token ) -> bool {
        ( self.pos == rhs.pos ) && ( self.tkn == rhs.tkn )
    }
    fn ne( & self, rhs: & Token ) -> bool {
        ( self.pos != rhs.pos ) || ( self.tkn != rhs.tkn )
    }
}

impl fmt::Display for Token {
    fn fmt( & self, fmt: & mut fmt::Formatter ) -> fmt::Result {
        write!(fmt, "{} | \"{}\"", self.pos, self.tkn)
    }
}

/// A lexer is a list of tokens.
pub struct Lexer {
    // The list of tokens.
    tokens: Vec<Token>,
}

impl Lexer {

    // Converts a string to a list of tokens and appends it to a prefix list of
    // token.
    fn line_to_tokens<F> (
        mut tokens: Vec<Token>,
        row: usize,
        is_whitespace: & F,
        string: & String,
    ) -> Vec<Token>
    where F: Fn(char) -> bool {

        let mut tkn = "".to_string() ;

        // Folding over the characters in `string`. The `col` accumulator is
        // the column of the first character of the current token.
        let col = string.chars().fold(

            // Starting at column 1.
            1,

            |col, ch4r| {

                if is_whitespace(ch4r) {
                    // Char is whitespace, appending token if its not empty.
                    if tkn.is_empty() {
                        // Empty token, incrementing col.
                        col + 1
                    } else {
                        // Char is whitespace and token is not empty. Pushing
                        // token on the list.
                        tokens.push( Token::of_ints(row,col,tkn.clone()) ) ;
                        // Incrementing col by the length of the token.
                        let col = col + tkn.len() ;
                        // Emptying token.
                        tkn.clear() ;
                        // Returning new column index.
                        col
                    }
                } else {
                    // Char is not whitespace, appending to current token.
                    tkn.push(ch4r) ;
                    // Column is unchanged.
                    col
                }
            }
        ) ;

        // Checking is there is a token left.
        if ! tkn.is_empty() {
            // Appending it to the list of tokens.
            tokens.push( Token::of_ints(row,col,tkn.clone()) )
        } ;

        tokens
    }

    /// Creates a lexer from a list of strings.
    pub fn of_lines<F>(
        lines: & Vec<String>, is_whitespace: & F
    ) -> Self

    where F: Fn(char) -> bool {
        let (tokens, _) =
            lines.iter().fold(
                (Vec::new(), 1),
                |(vec, row_count), line| (
                    Lexer::line_to_tokens(
                        vec, row_count, is_whitespace, line
                    ),
                    row_count + 1
                )
            ) ;
        Lexer { tokens: tokens }
    }

    /// Creates a lexer from a string.
    pub fn of_string<F> (
        string: & String, is_whitespace: & F
    ) -> Self

    where F: Fn(char) -> bool {
        Lexer::of_lines(
            & string
                .lines()
                .map(|s| s.to_string())
                .collect(),
            is_whitespace
        )
    }

    /// Creates a lexer from a file.
    pub fn of_file<F> (
        file: & mut File, is_whitespace: & F
    ) -> Self

    where F: Fn(char) -> bool {
        let mut string = String::new() ;
        file.read_to_string(& mut string).unwrap() ;

        Lexer::of_string(
            & string,
            is_whitespace
        )
    }


    /// Creates a lexer from a path to a file.
    pub fn of_path<F> (
        path: & Path, is_whitespace: & F
    ) -> Self

    where F: Fn(char) -> bool {
        let mut file = File::open(path).unwrap() ;
        Lexer::of_file( & mut file, is_whitespace )
    }

    /// The number of tokens in this lexer.
    pub fn len( & self ) -> usize {
        self.tokens.len()
    }

    /// Prints one token per line.
    pub fn print( & self ) {
        // Counter to display token index.
        let mut index = 0 ;
        for token in self.tokens.iter() {
            // Printing each line with position and content.
            println!("  {:>3} {}", index, token) ;
            index += 1 ;
        }
    }
}

// `Index` implementation for `Lexer` for easy access to tokens.
impl ops::Index<usize> for Lexer {
    type Output = Token ;
    fn index(
        & self, index: usize
    ) -> & Token {
        & self.tokens[index]
    }
}

/// A cursor on a `Lexer`.
pub struct LexerCursor {
    // The lexer.
    lexer: Rc<Lexer>,
    // The index of the current token.
    cursor: usize,
}

impl LexerCursor {

    /// Creates a `LexerCursor` pointing at the first token of a
    /// `Lexer`, from a `Rc<Lexer>`.
    pub fn mk(
        lexer: Rc<Lexer>
    ) -> LexerCursor {
        LexerCursor { lexer: lexer.clone(), cursor: 0 }
    }

    /// Creates a `LexerCursor` pointing at the first token of a
    /// `Lexer`.
    pub fn of_lexer(
        lexer: Lexer
    ) -> LexerCursor {
        LexerCursor { lexer: Rc::new(lexer), cursor: 0 }
    }

    /// Returns the current token pointed at by the cursor.
    pub fn read( & self ) -> Option<Token> {
        let len = self.lexer.len() ;
        let current = self.cursor ;
        if current >= len {
            // No more tokens.
            None
        } else {
            // There are some tokens left.
            Some( self.lexer[current].clone() )
        }
    }

    /// Returns true iff the lexer is at eof.
    pub fn is_at_eof( & self ) -> bool {
        self.cursor >= self.lexer.len()
    }

    /// Returns the position of the current token.
    pub fn pos( & self ) -> Option<Pos> {
        let len = self.lexer.len() ;
        let current = self.cursor ;
        if current >= len {
            // No more tokens.
            None
        } else {
            // Cloning position of current token.
            Some( self.lexer[current].pos.clone() )
        }
    }

    /// Creates a new `LexerCursor` with a different timeline.
    pub fn cp( & self ) -> Self {
        LexerCursor {
            lexer: self.lexer.clone(), cursor: self.cursor
        }
    }

    /// Eats a token (moves the cursor).
    pub fn eat( & self ) -> Self {
        LexerCursor {
            lexer: self.lexer.clone(), cursor: self.cursor + 1
        }
    }

    /// Resets the lexer.
    pub fn reset( & self ) -> Self {
        LexerCursor {
            lexer: self.lexer.clone(), cursor: 0
        }
    }

    /// Prints the complete list of tokens, with position..
    pub fn print( & self ) {
        // Printing cursor position.
        println!("Lexer@{}", self.cursor) ;
        // Printing tokens.
        self.lexer.print()
    }
}