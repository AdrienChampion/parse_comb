#![doc="
A purely functional parser combinators library. Uses the `Lexer` and
`LexerCursor` from the `lexer` module.

The standard way to create parsers is to use the `parser` macro,
which look like this:

```ignore
parser!(
    fn ident : Type { combinator_parser }
) ;
```

This creates a function called `ident` taking a `& LexerCursor` and
returning a `parser::Result<Type>`. This result type is either

* `Ok( result, lexer )` where `result` is the result and `lexer` is
  the new state of the lexer, or
* `Err( failure )` where failure is a `ParseFailure`, *i.e.*
  * `Msg( string )` with `string` a description of the failure, or
  * `EoF` meaning the parser reached end of file before it was done.

It is recommended to use the `parse` and `parse_all` functions when
actually parsing something. They take a `Lexer` and return a
`std::result::Result<Type, ParseFailure>`.

The following section introduces the syntax for `combinator_parser`.



# Syntax

The following parser combinators can be nested arbitrarily, except
obviously tokens and identifiers. There is **no** priority between
combinators so any parser combination must be parenthesized to be
combined with something else. In the following, syntax snippets of
the form `( <comb> )` stand for a combination of parsers between
parens or a token / identifier.

**Tokens** are pretty straightforward: `[\"bla\"]` parses a token
(`bla` here) and returns a `lexer::Token`.

**Regex** look like `{r\"^\\d{4}-\\d{2}-\\d{2}$\"}`. They use the
`regex` lib from the rust distribution, refer to
[its documentation][regex_doc] for more details.

[regex_doc]: http://doc.rust-lang.org/regex/regex/index.html
(Documentation of the regex crate.)

**Identifiers** refer to previously defined parsers: `my_parser`.

**Repetitions** of zero or more matches `( <comb> )*` or of one
or more matches `( <comb> )+`. If successful, it produces a
vector of whatever the type of `<comb>` is.

**Branches** are kind of Caml style:

```c,ignore
| (<comb_1>)
| (<comb_2>)
...
| (<comb_n>)
```

Note that all branches must be of the same type for the resulting
parser to make sense.

**Sequences** of parsers are comma separated `(<comb_1>), (<comb_2>),
...)` and, if successful, produce tuples of the expected type.

**Ignoring** part of a sequence is done with the `>` and `<` symbols:

```c,ignore
(<ignored>  ) > (<kept>)
                (<kept>) < (<ignored>  )
(<ignored_1>) > (<kept>) < (<ignored_2>)
```
When successful, the result has the type of `<kept>`.

**Modification by a function** is specified in three parts separated
by `=>`: `(<comb>) => <binding> => <block>`.

* `(<comb>)` is a combination of parsers,
* `<binding>` binds / destructures what `(<comb>)` returns, and
* `<block>` is a block using the bindings from `<binding>` to produce
  some value.

**N.B.** The arity of the *sequence* and *branch* combinators is
limited to eleven.



# The `Parseable` trait

Note that a `Parseable` trait is defined in `io` for `& str`,
`String`, `Vec<String>`, `std::path::Path`, and `std::fs::File`. It
implements the `parsex` and `parsex_all` functions for these types.



# Examples

Parsing a non-empty repetition of `\"bla\"` between matching curly braces.

```rust
#[macro_use(parser)]
extern crate parse_comb ;

use parse_comb::{ Parseable, Token } ;

fn main() {

    parser!(
        fn ocb_blas_ccb: (Token, Vec<Token>, Token) {
            [\"{\"], ([ \"bla\" ] +), [\"}\"]
        }
    ) ;

    match (
        \"{ bla bla }\".parsex( ocb_blas_ccb ),
        \"{ }\"        .parsex( ocb_blas_ccb ),
        \"{ bli bli }\".parsex( ocb_blas_ccb ),
    ) {
        ( Ok( (_, v2, _) ), Err(_), Err(_) ) => {
            assert!( v2.len() == 2 ) ;
        },
        _ => assert!( false ),
    }

}
```

Possibly empty repetition of `\"bla\"`, ignoring the braces.

```rust
#[macro_use(parser)]
extern crate parse_comb ;

use parse_comb::{ Parseable, Token } ;

fn main() {

    parser!(
        fn ocb_blas_ccb: Vec<Token> {
            [\"{\"] > ([ \"bla\" ] *) < [\"}\"]
        }
    ) ;

    match (
        \"{ bla bla bla }\".parsex( ocb_blas_ccb ),
        \"{ }\"            .parsex( ocb_blas_ccb ),
        \"{ {}}}bli bli }\".parsex( ocb_blas_ccb ),
    ) {
        ( Ok(v3), Ok(v0), Err(_) ) => {
            assert!( v3.len() == 3 ) ;
            assert!( v0.len() == 0 ) ;
        },
        _ => assert!( false ),
    }
}
```

Some more examples.

```rust
#[macro_use(parser)]
extern crate parse_comb ;

use parse_comb::{ Parseable, Token } ;

fn main() {

    parser!( fn ocb : Token { [ \"{\" ] } ) ;
    parser!( fn ccb : Token { [ \"}\" ] } ) ;

    parser!(
        fn cb_blas_blis_branch: (Token, Vec<Token>, Token) {
            | ( ocb, ( [\"bli\"] * ), ccb )
            | ( ocb, ( [\"bla\"] * ), ccb )
        }
    ) ;

    // Droping braces using a function, note the destructuring binding.
    parser!(
        fn blas_blis_branch: Vec<Token> {
            cb_blas_blis_branch => (_, vec, _) => { vec }
        }
    ) ;

    match (
        \"{ bla bla bla }\".parsex( blas_blis_branch ),
        \"{ bli }\"        .parsex( blas_blis_branch ),
        \"{ bli bla bli }\".parsex( blas_blis_branch ),
    ) {
        ( Ok(v3), Ok(v1), Err(_) ) => {
            assert!( v3.len() == 3 ) ;
            assert!( v1.len() == 1 ) ;
        },
        _ => assert!( false ),
    }


    // A bigger, stupidly monolithic, example.

    fn token_printer( tkn: & Token ) {
        println!(\"  {}\", tkn)
    }

    fn token_vec_printer( vec: & Vec<Token> ) {
        for tkn in vec.iter() {
            token_printer(tkn)
        }
    }

    parser!(
        fn stupid_parser: Vec<Vec<Token>> {
            (
                // We are parsing a `*` repetition of the following.
                // Parsed but ignored ocb.
                [\"{\"] > (
                    | (
                        // First branch, a `+` rep of `\"bla\"`.
                        ( [\"bla\"] + ) => vec => {
                            println!(\"Branch1:\") ;
                            println!(\"> vec:\") ;
                            token_vec_printer(& vec) ;
                            vec
                        }
                    )
                    | (
                        // Second branch, a `*` rep of `\"bli\"` and a `\"blu\"`.
                        ( ([\"bli\"] *), [\"blu\"] )
                        // Binding things.
                        => (vec, token) => {
                            println!(\"Branch2:\") ;
                            println!(\"> vec:\") ;
                            token_vec_printer(& vec) ;
                            println!(\"> token:\") ;
                            token_printer(& token) ;
                            // Actually dropping the `\"blu\"`.
                            vec
                        }
                    )
                    | (
                        // Third branch, a `\"bli\"` and a `*` rep of `\"bla\"`.
                        ([\"bli\"], ([\"bla\"] *) )
                        => (_, vec) => {
                            println!(\"Branch3:\") ;
                            println!(\"  (discarded first token)\") ;
                            println!(\"> vec:\") ;
                            token_vec_printer(& vec) ;
                            // Only returning the repetition.
                            vec
                        }
                    )
                // Parsed but ignored ccb with some side effects.
                ) < ([\"}\"] => token => {
                    println!(\"Closing curly brace is ignored but parsed:\") ;
                    token_printer(& token)
                })

            // We are indeed parsing a `*` rep of the body above.
            ) *
        }
    ) ;

    match \" { bli bla bla } { bli bli bli blu } { bla } { blu }\"
            .parsex( stupid_parser ) {

        Ok( vec_vec ) => {
            assert!( vec_vec.len() == 4 ) ;

            assert!( vec_vec[0].len() == 2 ) ;
            for bla in vec_vec[0].iter() {
                assert!( bla.tkn == \"bla\" ) ;
            } ;

            assert!( vec_vec[1].len() == 3 ) ;
            for bli in vec_vec[1].iter() {
                assert!( bli.tkn == \"bli\" ) ;
            } ;

            assert!( vec_vec[2].len() == 1 ) ;
            for bla in vec_vec[2].iter() {
                assert!( bla.tkn == \"bla\" ) ;
            } ;

            assert!( vec_vec[3].len() == 0 ) ;
        },
        _ => assert!( false ),
    }
}

```

A last example with option and regex.

```rust
#[macro_use(parser)]
extern crate parse_comb ;

use parse_comb::{ Parseable, Token } ;

fn main() {

    parser!( fn abca : Token { { r\"^a[b|c]*a$\" } } ) ;
    parser!( fn abdca : Token { { r\"^a[b|c]*d[b|c]*a$\" } } ) ;
    parser!( fn opt : (Option<Token>, Option<Token>) { (abca?), (abdca?) } ) ;

    let s = \"abcccbbbbcba\" ;
    match s.parsex( opt ) {
        Ok( (Some(t), None) ) => assert_eq!(s.len(), t.tkn.len()),
        _ => assert!(false),
    }
    let s = \"abcccbbbbcba abbcda\" ;
    match s.parsex( opt ) {
        Ok( (Some(t1), Some(t2)) ) => {
            let mut word_1 = s.to_string() ;
            let mut word_2 = \"\".to_string() ;
            loop {
                match word_1.pop() {
                    Some(' ') => break,
                    Some(c) => word_2.push(c),
                    None => assert!(false),
                }
            }
            assert_eq!(word_1.len(), t1.tkn.len()) ;
            assert_eq!(word_2.len(), t2.tkn.len())
        },
        _ => assert!(false),
    }
    let s = \"abcccbbbbcbaabbcda\" ;
    match s.parsex( opt ) {
        Ok( (None, None) ) => (),
        _ => assert!(false),
    }
    let s = \"abbcdbcbbbbcca\" ;
    match s.parsex( opt ) {
        Ok( (None, Some(t)) ) => assert_eq!(s.len(), t.tkn.len()),
        _ => assert!(false),
    }
}
```



"]
// #![feature(plugin)]
// #![plugin(regex_macros)]
extern crate regex ;

use std::result ;
use std::fs::File ;
use std::path::Path ;

/// A simple lexer.
pub mod lexer ;
/// Purely functional parser combinators library.
pub mod parser ;

// |===| Parseable trait.


pub use self::parser::{
    parse, parse_all, Result, ParseFailure
} ;
pub use self::lexer::Token ;
use self::lexer::{
    Lexer, LexerCursor, simple_is_whitespace
} ;

/// Allows parsing of some type.
pub trait Parseable {

    /// Parses something.
    fn parsex<T, F>(
        & self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> ;

    /// Parses something, fails if the parser terminates before its
    /// input ends.
    fn parsex_all<T, F>(
        & self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> ;
}

/// Allows parsing of some type, mutable version.
pub trait ParseableMut {

    /// Parses something.
    fn parsex<T, F>(
        & mut self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> ;

    /// Parses something, fails if the parser terminates before its
    /// input ends.
    fn parsex_all<T, F>(
        & mut self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> ;
}

impl Parseable for str {

    fn parsex<T, F>(
        & self,
        parser: F,
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_string(
            & self.to_string(),
            & simple_is_whitespace
        ) ;

        // Parsing.
        parse(lexer, parser)
    }

    fn parsex_all<T, F>(
        & self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_string(
            & self.to_string(),
            & simple_is_whitespace
        ) ;

        // Parsing all.
        parse_all(lexer, parser)
    }
}

impl Parseable for String {

    fn parsex<T, F>(
        & self,
        parser: F,
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_string(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing.
        parse(lexer, parser)
    }

    fn parsex_all<T, F>(
        & self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_string(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing all.
        parse_all(lexer, parser)
    }
}


impl Parseable for Vec<String> {

    fn parsex<T, F>(
        & self,
        parser: F,
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_lines(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing.
        parse(lexer, parser)
    }

    fn parsex_all<T, F>(
        & self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_lines(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing all.
        parse_all(lexer, parser)
    }
}


impl Parseable for Path {

    fn parsex<T, F>(
        & self,
        parser: F,
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_path(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing.
        parse(lexer, parser)
    }

    fn parsex_all<T, F>(
        & self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_path(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing all.
        parse_all(lexer, parser)
    }
}

impl ParseableMut for File {

    fn parsex<T, F>(
        & mut self,
        parser: F,
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_file(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing all.
        parse(lexer, parser)
    }

    fn parsex_all<T, F>(
        & mut self,
        parser: F
    ) -> result::Result<T, ParseFailure>
    where F: Fn(& LexerCursor) -> Result<T> {

        // Creating lexer.
        let lexer = Lexer::of_file(
            self,
            & simple_is_whitespace
        ) ;

        // Parsing all.
        parse_all(lexer, parser)
    }
}